// https://stackoverflow.com/questions/31495344/change-cursor-depending-on-section-of-canvas  HIGHLY PRAISED, MANY THANKS
//
// Position of on canvas click: patriques, https://stackoverflow.com/questions/55676/how-do-i-get-the-coordinates-of-a-mouse-click-on-a-canvas-element/18053642#18053642
// Dragging: Dobre, Ramtal - Physics for JavaScript Games, Animation, and Simulations: with HTML5 Canvas, Apress, 2014, pages 26-27
//
// n - num of discs, minimum number of moves: 2^n - 1
// * document.write('<h1>Towers of Hanoi - rules</h1>..._WHOLE_HTML_FILE'); */

// should you find non-English comments, skip them. (I may note st. in my mother language, here and there)

// show, hide the rules

function showHide() {	
	var rules = document.getElementById("rulesId");
	if (rules.style.display === "none") {
		rules.style.display = "block";
	}
	else {
		rules.style.display = "none";
	}
}
	
// globals: canvas related

var canvas = document.querySelector('#canvasId'); var width = canvas.width = window.innerWidth; var height = canvas.height = window.innerHeight; var ctx = canvas.getContext('2d'); var offsetX, offsetY;

// globals: pointer (mouse) related

var curPointer = "default";	// 'default', 'pointer', 'move'

// globals: other globals

var polesWithDiscs = [], discsDia = [], dragging = false, nAllDiscs = 10, n = 0;	// 'n' number of discs currently in the game

color = ['rgb(16, 209, 219)', 'rgb(255, 255, 102)', 'rgb(255, 153, 153)', 'rgb(204, 255, 102)', 'rgb(204, 51, 255)', 'rgb(0, 51, 102)',
'rgb(204, 102, 0)', 'rgb(204, 0, 0)', 'rgb(255, 153, 255)', 'rgb(153, 102, 0)', 'rgb(21, 81, 5)']; radius = [Math.floor(width/135.375), 1.5*Math.floor(width/135.375)]; var xSidePoleBase = Math.floor(width/16.4); yTopPoleBase = Math.floor( 2.1*width/5 ); yTopNewDiscBase = Math.floor( 0.438467221*width ) - 5*radius[0]; var dia = { max: Math.floor((width - 2*xSidePoleBase - 2*Math.floor( 1.1*width*37/1083 ))/3), min: Math.floor((width - 2*xSidePoleBase - 2*Math.floor( 1.1*width*37/1083 ))/18) };
for (let i = 0; i < nAllDiscs; i++) {
	if (i === 0) {discsDia.push(dia.min);}
	else {
		discsDia.push(Math.floor(discsDia[i - 1] + (dia.max - dia.min)/(nAllDiscs - 1)));
	}
}
var xMoveToNextP = dia.max + Math.floor( 1.1*width*37/1083 );	// move to the next pole (to the 2nd & 3rd)

// converting deg to rad angles (JS-understands rads)

function degToRad(degrees) {
	return Math.round((degrees * Math.PI / 180) * 100) / 100;	// Math.round(num * 100) / 100
}

// start CLASSES, prototypes

// 'top' level class Shape, constructora

function Shape(xMoveTo, yMoveTo, dia, rad, fillStyle, pole, levelFrBot, draggableDropTarget = false) {
	//console.log("discs.length: " + discs.length + " " + typeof discs.length);
	this.xMoveTo = xMoveTo;
	this.yMoveTo = yMoveTo;
	this.dia = dia;
	this.rad = rad;
	this.fillStyle = fillStyle;
	this.pole = pole;
	this.levelFrBot = levelFrBot;
	this.draggableDropTarget = draggableDropTarget;	// ADDED DRAG DROP FEATURE
}
/*
class Shape {
  constructor (xMoveTo, yMoveTo, dia, rad, fillStyle, pole, levelFrBot, draggableDropTarget = false) {
	this.xMoveTo = xMoveTo
	this.yMoveTo = yMoveTo
	this.dia = dia
	this.rad = rad
	this.fillStyle = fillStyle
	this.pole = pole
	this.levelFrBot = levelFrBot
	this.draggableDropTarget = draggableDropTarget	// ADDED DRAG DROP FEATURE
  }
}
*/
// define 'top - 1' level Pole constructor, inheriting from Shape
/*
class Pole extends Shape {
  constructor (xMoveTo, yMoveTo = yTopPoleBase, dia = dia.max, rad = radius[0], fillStyle = color[0], pole, poleHeight, draggableDropTarget = true) {
    super(xMoveTo, yMoveTo, dia, rad, fillStyle, pole, poleHeight, draggableDropTarget)
		this.poleHeight = Math.floor(width/3.4)
		this.pole = pole
		this.discsArray = []
		polesWithDiscs.push(this)
  }

  draw() {
    ctx.beginPath()
    ctx.strokeStyle = 'rgb(0, 0, 0)'	
    ctx.lineTo(this.xMoveTo + this.dia, this.yMoveTo)
    ctx.arc(this.xMoveTo + this.dia, this.yMoveTo - this.rad, this.rad, degToRad(90), degToRad(270), true)
    ctx.lineTo(this.xMoveTo + this.dia/2 + this.rad, this.yMoveTo - 2*this.rad)
    ctx.arc(this.xMoveTo + this.dia/2, this.yMoveTo - 2*this.rad - this.poleHeight, this.rad, degToRad(0), degToRad(180), true)
    ctx.lineTo(this.xMoveTo + this.dia/2 - this.rad, this.yMoveTo - 2*this.rad)
    ctx.arc(this.xMoveTo, this.yMoveTo - this.rad, this.rad, degToRad(270), degToRad(90), true)
    ctx.closePath()
    ctx.stroke()
    ctx.fillStyle = this.fillStyle
		ctx.fill()
	}
}
*/
function Pole(xMoveTo, yMoveTo = yTopPoleBase, dia = dia.max, rad = radius[0], fillStyle = color[0], pole, poleHeight, draggableDropTarget = true) {
 	Shape.call(this, xMoveTo, yMoveTo, dia, rad, fillStyle, pole, poleHeight, draggableDropTarget)
	this.yMoveTo = yTopPoleBase;
	this.dia = dia; // dia.max
	this.rad = radius[0]; // rad
	this.fillStyle = color[0]; // fillStyle
 	this.poleHeight = Math.floor(width/3.4);
 	this.pole = pole;
 	this.discsArray = [];
 	polesWithDiscs.push(this);
}
Pole.prototype = Object.create(Shape.prototype);//??????????????????????????????????????
Pole.prototype.constructor = Pole;
// define 'top - 1' level Disc constructor, inheriting from Shape
/*
class Disc extends Shape {
	constructor (xMoveTo, yMoveTo, dia, rad, fillStyle, pole, levelFrBot) {
		super(xMoveTo, yMoveTo, dia, rad, fillStyle, pole, levelFrBot)
	}

	draw() {
		ctx.beginPath()
		ctx.moveTo(this.xMoveTo, this.yMoveTo)
		ctx.arc(this.xMoveTo + this.dia, this.yMoveTo - this.rad, this.rad, degToRad(90), degToRad(270), true)
		ctx.arc(this.xMoveTo, this.yMoveTo - this.raid, this.rad, degToRad(270), degToRad(90), true)
		ctx.closePath()
		ctx.fillStyle = this.fillStyle
		ctx.fill()
		ctx.stroke()
	}
}
*/
function Disc(xMoveTo, yMoveTo, dia, rad, fillStyle, pole, levelFrBot) {
  Shape.call(this, xMoveTo, yMoveTo, dia, rad, fillStyle, pole, levelFrBot)
  this.coords = (xMoveTo, yMoveTo, dia);
}
Disc.prototype = Object.create(Shape.prototype);
Disc.prototype.constructor = Disc;

// prototypes (pole and disc)
Disc.prototype.draw = function(flag) {  // flag 'true' - discs drawing from Anim1 -> OnEachStep, 'false' - from elsewhere
  ctx.beginPath();
	ctx.moveTo(this.xMoveTo, this.yMoveTo);
	ctx.arc(this.xMoveTo + this.dia, this.yMoveTo - this.rad, this.rad, degToRad(90), degToRad(270), true);
	ctx.arc(this.xMoveTo, this.yMoveTo - this.rad, this.rad, degToRad(270), degToRad(90), true);
	if (flag && this.draggableDropTarget) {
		//	https://stackoverflow.com/questions/2601097/how-to-get-the-mouse-position-without-events-without-moving-the-mouse
		if (ctx.isPointInPath(mouseX, mouseY) && curPointer === "default") {
			curPointer = "pointer";
			canvas.style.cursor = curPointer; 
		}
	}
 	ctx.closePath();
	ctx.fillStyle = this.fillStyle;
	ctx.fill();
 	ctx.stroke();
}

Pole.prototype.draw = function() {
	ctx.beginPath();
 	ctx.strokeStyle = 'rgb(0, 0, 0)';	
	ctx.lineTo(this.xMoveTo + this.dia, this.yMoveTo);
	ctx.arc(this.xMoveTo + this.dia, this.yMoveTo - this.rad, this.rad, degToRad(90), degToRad(270), true);
	ctx.lineTo(this.xMoveTo + this.dia/2 + this.rad, this.yMoveTo - 2*this.rad);
	ctx.arc(this.xMoveTo + this.dia/2, this.yMoveTo - 2*this.rad - this.poleHeight, this.rad, degToRad(0), degToRad(180), true);
	ctx.lineTo(this.xMoveTo + this.dia/2 - this.rad, this.yMoveTo - 2*this.rad);
	ctx.arc(this.xMoveTo, this.yMoveTo - this.rad, this.rad, degToRad(270), degToRad(90), true);
  ctx.closePath();
	ctx.stroke();
	ctx.fillStyle = this.fillStyle;
	ctx.fill();
}

// end CLASSES, prototypes

// start player's SETUP

// discs are created here:
function addKillDiscs(a_s_Sign) {	// a_s_Sign: add (1), subtract (-1) flag
  n = polesWithDiscs[0].discsArray.length;	// local 'n'
  if (a_s_Sign === -1) { // removing the bottom disc, pushing all others one level down
		polesWithDiscs[0].discsArray.pop(); // removing the bottom disc from the 'pole1discs' array,
		if (n > 1) {
		  n -= 1;
			// alert ("Just removed the bottom disc. And decremented 'n' by 1.			n = " + n);
			if (n == 2) {
				stopAnim();
				canvas.removeEventListener('mousedown', mainEvHandler, false)
				/* if (n == 0) {canvas.removeEventListener('mousedown', MATCH PROBLEM: evHhandler(e)); */
				document.removeEventListener('mousemove', evHandler, false);
				document.removeEventListener('mouseup', evHandler, false);
			} // else {
			for (let i = 0; i < n; i++) {
				// moving each non-bottom disc object one level down
				polesWithDiscs[0].discsArray[i].yMoveTo -= (2*radius[1]*a_s_Sign + 2*a_s_Sign);
				polesWithDiscs[0].discsArray[i].levelFrBot += a_s_Sign;
			}
		}
	} else { // adding new bottom disc, pulling all others one level up
		var disc = new Disc(	// adding new bottom disc object
			xSidePoleBase + (1/2)*(dia.max - discsDia[n]), //xMoveTo
			yTopNewDiscBase,// - n*2*radius[1], //yMoveTo
			discsDia[n], //discsDia.slice(nAllDiscs - newV, )[i], //dia
			radius[1], //rad
			color[n + 1], //fillStyle; the very first 0th color is for the poles
			0, //pole
		  0, //levelFrBot
			// draggableDropTarget 'false', EXCEPT for the very 1st disc, which is 'true' // ADDED DRAGGABILITY FEATURE
		);
		if (n > 0) {
			// moving each disc object already in existence one level up 
			for (let i = 0; i < n; i++) {
				polesWithDiscs[0].discsArray[i].yMoveTo -= (2*radius[1]*a_s_Sign + 2*a_s_Sign);
				polesWithDiscs[0].discsArray[i].levelFrBot += a_s_Sign;
			}
		} else { // i.e. if n == 0
			disc.draggableDropTarget = true; // the very top disc has 'true'
		}
		polesWithDiscs[0].discsArray.push(disc);
		n += 1;
	}
	if (n >= 3) { startAnim1__isPointInPath_listen4mousedown() }
	drawDiscs(false);		// draws Poles, draws all the discs in their updated levels
}

function addOneDisc() {
	var i = parseInt(document.getElementById("numDiscs").innerHTML, 10);
	if (i >= 0 && i < 10) {
	 	addKillDiscs(1);	// '1' - Add
		return document.getElementById("numDiscs").innerHTML = String(i + 1);
	} else {;}
}

function subtrOneDisc() {
	var i = parseInt(document.getElementById("numDiscs").innerHTML, 10);
	if (i > 0) {
		addKillDiscs(-1);	// '-1' - Subtract
		return document.getElementById("numDiscs").innerHTML = String(i - 1);
	}
}

// end player's SETUP

function offset() {
  var rect = canvas.getBoundingClientRect();
  offsetX = rect.left;
  offsetY = rect.top;
}
offset();
window.onscroll=function(e){ offset(); }
window.onresize=function(e){ offset(); }

// poles created & drawn, discs drawn

function createPoles() {
	for (let i=0; i < 3; i++) {
		var pole = new Pole( // xMoveTo, yMoveTo, dia, rad, fillStyle, pole, poleHeight
			xSidePoleBase + i * xMoveToNextP, // xMoveTo
			yTopPoleBase, // yMoveTo
			dia.max, // dia
			radius[0], // radius
			color[0], // fillStyle
			pole = i, // pole
			Math.floor(width/3.4), // poleHeight
			draggableDropTarget = true // ADDED DROP FEATURE
		)
	}
	drawPoles();
	//alert("N children of Body: " + document.body.children.length);
	return document.getElementById("numDiscs").innerHTML = String(0);
}

function drawPoles() {
	ctx.clearRect(0, 0, width, height);
  for (let i = 0; i < polesWithDiscs.length; i++) {
		polesWithDiscs[i].draw();
	}
}

// flag 'true' - discs drawing called from Anim1 -> OnEachStep, flag 'false' - discs drawing called from elsewhere

function drawDiscs(flag) {		// draws Poles, draws Discs
  ctx.clearRect(0, 0, width, height);
  drawPoles();
  for (let i = 0; i < polesWithDiscs[0].discsArray.length; i++) {
    polesWithDiscs[0].discsArray[i].draw(flag);
  }	 
}

function onEachStep() {
	drawDiscs(true);
}

// Anim1 - mouse pointer = 'pointer', listen for 'mousedown'. Anim2 - disc being dragged & mouse pointer = 'move'.

function startAnim1__isPointInPath_listen4mousedown() {
	interval = setInterval(onEachStep, 1000/60); // 60 fps | 'pointer' goes into 'onEachStep()'
	setListeners();
}

function setListeners() {
  //targetElm.handleEvent(event) is built-in
   if (!dragging) {	// don't put this inside 'startAnim1__isPointInPath_listen4mousedown' - it causes strange error
     canvas.addEventListener('mousedown', mainEvHandler, false);
	}
}

function stopAnim() {
	clearInterval(interval);
}

// start EVENT HANDLERS

function mainEvHandler(e) {
				dragging = true;
				// startAnim2();
				document.addEventListener('mousemove', evHandler, false);
				document.addEventListener('mouseup', evHandler, false);
				// stopAnim2();
}

function evHandler(e) {
	// var rect = canvas.getBoundingClientRect();
	// not dragging => 'default' mouse cursor turns 'pointer' - when placed over draggable discs
	if (!dragging) {
		e.preventDefault();
		e.stopPropagation();
		mouseX = parseInt(e.clientX - offsetX);
		mouseY = parseInt(e.clientY - offsetY);

		switch(e.type) {
			case 'mouseover':
		    alert("mouse over");
       	break;
		}
	}
	// dragging 
	else {
		switch(e.type) { 
			case 'mousemove':
				//    some code here...
				if (dragging) {
					alert(e.type);
				}
				break;

			case 'mouseup':
				if (dragging) {
					alert(e.type);
					dragging = false;
					document.removeEventListener('mousemove', evHandler, false);
					document.removeEventListener('mouseup', evHandler, false);
					// some other code here...
				}
				break;
  	}
	}
}

// end EVENT HANDLERS

// init

createPoles();	// get poles created and drawn

// ctx.beginPath();
// ctx.rect(0, 0, 100, 150);
// p1 = ctx.currentPath;
// ctx.fill(p1);
// console.log("line 379: " + ctx.isPointInPath(30, 70));
// 		ctx.isPointInPath( mouseX, mouseY )
